<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * Sizes
 *
 * @ORM\Table(name="sizes")
 * @ORM\Entity
 */
class Sizes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;


}
