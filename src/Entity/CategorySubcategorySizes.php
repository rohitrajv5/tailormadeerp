<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * CategorySubcategorySizes
 *
 * @ORM\Table(name="category_subcategory_sizes", indexes={@ORM\Index(name="category_subcategory_id", columns={"category_subcategory_id"}), @ORM\Index(name="size_id", columns={"size_id"})})
 * @ORM\Entity
 */
class CategorySubcategorySizes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="fabric_length", type="float", precision=10, scale=2, nullable=false)
     */
    private $fabricLength;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \CategorySubcategories
     *
     * @ORM\ManyToOne(targetEntity="CategorySubcategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_subcategory_id", referencedColumnName="id")
     * })
     */
    private $categorySubcategory;

    /**
     * @var \Sizes
     *
     * @ORM\ManyToOne(targetEntity="Sizes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="size_id", referencedColumnName="id")
     * })
     */
    private $size;


}
