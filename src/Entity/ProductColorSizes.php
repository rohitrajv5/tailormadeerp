<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductColorSizes
 *
 * @ORM\Table(name="product_color_sizes", indexes={@ORM\Index(name="product_id", columns={"product_id"}), @ORM\Index(name="color_id", columns={"color_id"}), @ORM\Index(name="size_id", columns={"size_id"})})
 * @ORM\Entity
 */
class ProductColorSizes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="base_cost", type="float", precision=10, scale=2, nullable=false)
     */
    private $baseCost;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_cost", type="float", precision=10, scale=2, nullable=false)
     */
    private $sellCost;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_stock", type="integer", nullable=true)
     */
    private $isStock;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_feature", type="integer", nullable=true)
     */
    private $isFeature;

    /**
     * @var string
     *
     * @ORM\Column(name="bar_code", type="string", length=100, nullable=false)
     */
    private $barCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var \Colors
     *
     * @ORM\ManyToOne(targetEntity="Colors")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     * })
     */
    private $color;

    /**
     * @var \Products
     *
     * @ORM\ManyToOne(targetEntity="Products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Sizes
     *
     * @ORM\ManyToOne(targetEntity="Sizes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="size_id", referencedColumnName="id")
     * })
     */
    private $size;


}
