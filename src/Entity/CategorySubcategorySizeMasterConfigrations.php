<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * CategorySubcategorySizeMasterConfigrations
 *
 * @ORM\Table(name="category_subcategory_size_master_configrations", indexes={@ORM\Index(name="master_configration_id", columns={"master_configration_id"}), @ORM\Index(name="category_subcategory_size_id", columns={"category_subcategory_size_id"})})
 * @ORM\Entity
 */
class CategorySubcategorySizeMasterConfigrations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \CategorySubcategorySizes
     *
     * @ORM\ManyToOne(targetEntity="CategorySubcategorySizes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_subcategory_size_id", referencedColumnName="id")
     * })
     */
    private $categorySubcategorySize;

    /**
     * @var \MasterConfigrations
     *
     * @ORM\ManyToOne(targetEntity="MasterConfigrations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="master_configration_id", referencedColumnName="id")
     * })
     */
    private $masterConfigration;


}
