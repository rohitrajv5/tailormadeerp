<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * CategorySuperCategories
 *
 * @ORM\Table(name="category_super_categories", indexes={@ORM\Index(name="super_category_id", columns={"super_category_id"}), @ORM\Index(name="category_id", columns={"category_id"})})
 * @ORM\Entity
 */
class CategorySuperCategories
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    private $createdAt;

    /**
     * @var \Categories
     *
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \SuperCategories
     *
     * @ORM\ManyToOne(targetEntity="SuperCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="super_category_id", referencedColumnName="id")
     * })
     */
    private $superCategory;


}
