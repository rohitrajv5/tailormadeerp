<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * CategorySubcategories
 *
 * @ORM\Table(name="category_subcategories", indexes={@ORM\Index(name="category_id", columns={"category_id"}), @ORM\Index(name="subcategory_id", columns={"subcategory_id"})})
 * @ORM\Entity
 */
class CategorySubcategories
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \Categories
     *
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Subcategories
     *
     * @ORM\ManyToOne(targetEntity="Subcategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subcategory_id", referencedColumnName="id")
     * })
     */
    private $subcategory;


}
